import React from 'react';
import { render } from 'react-dom';
import { AppComponent } from './App.component';
import { createRenderer } from './three/createRenderer';

render(

<AppComponent />,

 document.getElementById('ui')
);

export const renderer = createRenderer();
