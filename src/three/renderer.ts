/* eslint-disable @typescript-eslint/no-unsafe-assignment */
import {
  Color,
  LinearToneMapping,
  PerspectiveCamera,
  PointLight,
  Scene,
  Vector2,
  WebGLRenderer,
} from "three";
import { Style } from "./style";
import { Terrain } from "./terrain";
import { Dune } from "./dune";

export class Renderer {
  private style: Style;

  private light: PointLight;
  private renderer: WebGLRenderer;
  private camera: PerspectiveCamera;
  private scene: Scene;
  private canvas: HTMLCanvasElement;
  private body: HTMLElement;

  private move: Vector2;
  private mouse: Vector2;
  private scrollY = 0;
  private scrollYMax = 0;
  private width = 0;
  private height = 0;
  private time = 0;

  private terrain: Terrain;
  private dune: Dune;

  constructor(style: Style) {
    this.style = style;

    this.body = document.body;
    this.width = this.body.offsetWidth;
    this.height = this.body.offsetHeight;

    this.canvas = <HTMLCanvasElement>document.getElementById("mainCanvas");

    this.mouse = new Vector2(this.width / 2, this.height / 2);
    this.move = new Vector2(0, 0);

    this.renderer = new WebGLRenderer({
      antialias: true,
      alpha: false,
      powerPreference: "high-performance",
      canvas: this.canvas,
    });
    this.renderer.setSize(this.width, this.height);
    this.renderer.setPixelRatio(window.devicePixelRatio);
    this.renderer.toneMapping = LinearToneMapping;
    this.renderer.setClearColor(this.style.backgroundColor, 0.0);

    this.camera = new PerspectiveCamera(
      this.style.cameraPerspective,
      this.width / this.height,
      0.1,
      1000
    );
    this.camera.position.set(0, 0, 500);
    this.camera.rotation.set(0, 0, 0);

    this.scene = new Scene();
    this.scene.background = new Color(this.style.backgroundColor);
    this.scene.add(this.camera);

    this.terrain = new Terrain(
      this.style.foregroundColor,
      this.style.opacity,
      this.style.blending
    );
    // this.scene.add(this.terrain);

    this.dune = new Dune(this.width, this.height);
    this.scene.add(this.dune);

    this.light = new PointLight(
      this.style.lightColor,
      this.style.lightStrenght,
      1000,
      2
    );
    this.light.position.set(0, 0, 250);
    this.light.castShadow = false;
    this.scene.add(this.light);

    this.render();
  }

  public resize(): void {
    if (
      this.width != this.body.offsetWidth ||
      this.height != this.body.offsetHeight
    ) {
      this.width = this.body.offsetWidth;
      this.height = this.body.offsetHeight;

      this.renderer.setSize(this.width, this.height);

      this.camera.aspect = this.width / this.height;
      this.camera.updateProjectionMatrix();
    }
  }

  public setMouse(mouseX: number, mouseY: number): void {
    this.mouse = new Vector2(mouseX, mouseY);
  }

  public setMovement(move: Vector2): void {
    this.move = move;
  }

  public destroy(): void {
    this.renderer.forceContextLoss();
  }

  private update(): void {
    this.terrain.update(this.move);
    this.time += 10;
    this.dune.update(this.mouse, this.time);
  }

  private render(): void {
    this.update();
    this.renderer.render(this.scene, this.camera);
    requestAnimationFrame(() => this.render());
  }
}
