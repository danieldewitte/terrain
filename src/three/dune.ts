import { Mesh, PlaneGeometry, ShaderMaterial, Vector2, Vector3 } from "three";
export class Dune extends Mesh {
  constructor(width: number, height: number) {
    super();

    this.geometry = new PlaneGeometry(width * 1.4, height * 1.4, 1, 1);

    this.material = new ShaderMaterial({
      uniforms: {
        iTime: { value: 0.00001 },
        iResolution: { value: new Vector3(1, 1, 1) },
        iMouse: { value: new Vector2(0, 0) },
      },
      vertexShader: VERTEX,
      fragmentShader: FRAGMENT,
    });
  }

  public update(mouse: Vector2, time: number): void {
    const mat = this.material as ShaderMaterial;
    if (mat.uniforms) {
      const itime = mat.uniforms["iTime"];
      if (itime) itime.value = time * 0.001;

      if (mouse.x > 1 || mouse.x < -1) {
        const iMouse = mat.uniforms["iMouse"];
        if (iMouse) iMouse.value = mouse.multiplyScalar(0.001);
      }
    }
  }
}

const VERTEX = `
varying vec2 vUv;
void main() {
  vec4 mvPosition = modelViewMatrix * vec4(position, 1.);
  gl_Position = projectionMatrix * mvPosition;
  vUv = uv;
}`;

const FRAGMENT = `
uniform float iTime;
uniform vec2 iResolution;
uniform vec2 iMouse;
varying vec2 vUv;

float map(vec3 p) {
  p.z += iTime;
  float h = dot(sin(p - cos(p.yzx*1.3)), vec3(.1));
  h += dot(sin(p*2. - cos(p.yzx*1.3*2.)), vec3(.1/2.));
  return p.y + 1.1 + h;     
}

vec3 getNormal(vec3 p) {
  vec2 e = vec2(0.35, -0.35); 
  return normalize(
      e.xyy * map(p + e.xyy) + 
      e.yyx * map(p + e.yyx) + 
      e.yxy * map(p + e.yxy) + 
      e.xxx * map(p + e.xxx));
}

vec3 col(vec3 ro, vec3 rd, vec3 norm, float md, float t) {   
  vec3 ld = ro + vec3(-1.0, -1.0, 1.21); 
  float diff = max(dot(norm, ld), 0.0);
  float spec = pow(max( dot( reflect(-ld, norm), -rd ), 0.0 ), 7.0);
  vec3 objCol = vec3(0.3, 0.3,0.3);
  vec3 glowCol = vec3(1.0, 0.4, 0.0); 
  vec3 sceneCol = (objCol*(diff - 0.015 * 0.5) + vec3(1.0, 0.6, 0.2)*spec*0.15) ;
  sceneCol =  mix( sceneCol, vec3(0.55,0.9,0.9), 1.0 - exp( -0.00016*t*t*t ) );
  float sand = smoothstep(0.12, 1.15, 0.008 / md * t) * 0.3;
  sceneCol += glowCol * sand;
  return sceneCol;
}

void main() {
  vec2 uv = (vUv.xy-iResolution.xy*.55)/iResolution.y;
  vec3 ro = vec3(0.0, 0.0, 0.0); 
  vec3 rd = normalize(vec3(uv,1.0));
  float t = 0.0; 
  float minDist = 999.0;
  for (int i = 0; i < 100; i++) {
      float d = map(ro + rd*t);
      minDist = min(minDist, d);
      if(abs(d)<0.01) {
          minDist = 0.1;
          break;  
      }
      if(t>25.0) {
          minDist = min(minDist, d);
          t = 80.0;
          break;
      }
      t += d * 0.7;
  }
  vec3 norm = getNormal(ro + rd * t);
  vec3 sceneColor = col(ro, rd, norm, minDist, t);
  gl_FragColor = vec4(sqrt(clamp(sceneColor, 0.0, 1.0)), 1.0);
}`;
