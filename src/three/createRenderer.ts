import { Vector2 } from "three";
import { deviceInfo } from "../utils/deviceinfo";
import { Renderer } from "./renderer";

export function createRenderer(): Renderer {
  const renderer = new Renderer({
    cameraPerspective: 75,
    backgroundColor: 0x87ceeb,
    foregroundColor: 0x567d46,
    lightColor: 0xffffff,
    lightStrenght: 1,
    blending: true,
    bloom: false,
    bloomRadius: 1,
    bloomStrength: 1,
    bloomThreshold: 0.3,
    ease: 6,
    opacity: 1,
  });

  window.addEventListener("resize", () => {
    renderer.resize();
  });

  document.addEventListener("keydown", (e) => {
    switch (e.key) {
      case "ArrowLeft":
        renderer.setMovement(left);
        break;
      case "ArrowRight":
        renderer.setMovement(right);
        break;
      case "ArrowUp":
        renderer.setMovement(forward);
        break;
      case "ArrowDown":
        renderer.setMovement(backward);
        break;
    }
  });

  window.addEventListener("mousemove", (e) => {
    renderer.setMouse(deviceInfo.mouseCenterX(e), deviceInfo.mouseCenterY(e));
  });

  return renderer;
}

const forward = new Vector2(0.000005, 0);
const backward = new Vector2(-0.000005, 0);
const left = new Vector2(0, -0.000005);
const right = new Vector2(0, 0.000005);
