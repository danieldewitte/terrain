export interface Style {
  cameraPerspective: number;
  backgroundColor: number;
  foregroundColor: number;
  lightColor: number;
  lightStrenght: number;
  opacity: number;
  blending: boolean;
  ease: number;
  bloom: boolean;
  bloomThreshold: number;
  bloomStrength: number;
  bloomRadius: number;
}
