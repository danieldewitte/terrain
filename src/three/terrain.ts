import { makeNoise2D } from "fast-simplex-noise";
import {
  AdditiveBlending,
  BufferAttribute,
  FrontSide,
  Mesh,
  MeshPhongMaterial,
  NormalBlending,
  PlaneGeometry,
  Vector2,
  Vector3,
} from "three";

export class Terrain extends Mesh {
  private noise = makeNoise2D();
  private speed = 0.1;
  private speedVec = new Vector2(this.speed, this.speed);
  private pos = new Vector2(0, 0);
  private smoothness = 250;
  private scaleFactor = 30;

  private landPositions: BufferAttribute;
  private landPos: Array<number>;

  constructor(color: number, opacity: number, blending: boolean) {
    super();

    this.position.set(0, 0, 0);
    this.rotation.set(-1, 0, 0);

    this.geometry = new PlaneGeometry(2000, 1000, 64, 64);

    this.material = new MeshPhongMaterial({
      color: color,
      opacity: opacity,
      blending: blending ? AdditiveBlending : NormalBlending,
      side: FrontSide,
      transparent: true,
      depthTest: false,
      wireframe: true,
    });

    this.position.set(0, 0, 0);
    this.landPositions = this.geometry.getAttribute(
      "position"
    ) as BufferAttribute;
    this.landPos = this.landPositions?.array as Array<number>;
  }

  public update(move: Vector2): void {
    for (let i = 0; i < this.landPos.length; i += 3) {
      const v = new Vector3(
        this.landPos[i],
        this.landPos[i + 1],
        this.landPos[i + 2]
      );
      this.pos = this.pos.add(move);

      const xoff = v.x / this.smoothness + this.pos.y;
      const yoff = v.y / this.smoothness + this.pos.x;
      v.z = this.noise(xoff, yoff) * this.scaleFactor;

      this.landPos[i] = v.x;
      this.landPos[i + 1] = v.y;
      this.landPos[i + 2] = v.z;
    }

    this.landPositions.needsUpdate = true;
  }
}
